// import other routes
const userRoutes = require('./users');

const path = require('path');

const appRouter = (app, fs) => {

    // default route
    app.get('/', (req, res, next) => {
        return res.sendFile(path.join(__dirname + '/../public/index.html'));
    });

    // // other routes
    userRoutes(app, fs);

};

module.exports = appRouter;