## API Starter using Nodejs-Express

## Clone the repository

## Open the project in command prompt or terminal and type - npm install to install all dependencies

## To see the output, open http://localhost:3001 in your browser

## Api endpoints
### get:
### /users to get all users.
### /user/id to get a user by id

### post:
### /users to add a new user. Takes a json object in the request body

### put:
### /users/id to update a new user with id. Takes a json object in the request body

### delete:
### /users/id to delete user with specific id
